import sys
import os


if len(sys.argv) <= 1:
    print("""
Usage: 
    python to_one_line.py <private_key_file_name>
    """)
    sys.exit(1)

file_name = sys.argv[1]
print("Private Key File name : {}".format(file_name))

if os.path.exists(file_name):
    pass
else:
    print("Error! Given file: {} does not exist! Please check and try again!".format(file_name))
    sys.exit(1)

print("Reading File: {}".format(file_name))
with open(file_name, "r") as r:
    lines = r.read()

print("Writing to File: one_line_{}".format(file_name))
with open("one_line_{}".format(file_name), "w") as w:
    w.write("\\n".join(lines.splitlines()))
